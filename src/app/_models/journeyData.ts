import {Journey} from './journey';

export class JourneyData {
    id: number = 0;
    longitude: number;
    latitude: number;
    journey: Journey;
    time: Date;
}