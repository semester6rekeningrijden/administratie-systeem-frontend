export enum VehicleType {
    ALL,
    CAR,
    BUS,
    TRUCK
}