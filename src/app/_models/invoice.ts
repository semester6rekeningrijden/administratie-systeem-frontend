
import {User} from './user';
import {InvoiceItem} from './invoiceItem';
import {InvoiceStatus} from './invoiceStatus';

export class Invoice {
    public id: number;
    public invoiceNumber: number;
    public vehicleTrackerId: number;
    public invoiceStatus: InvoiceStatus;
    public totalPrice: number;
    public date: Date;
    public user: User;
    public invoiceItems: InvoiceItem[];
}