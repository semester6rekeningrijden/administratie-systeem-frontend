import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VehiclesRoutingModule} from './vehicles-routing.module';
import {AllvehiclesModule} from './allvehicles/allvehicles.module';
import {ViewvehicleModule} from './viewvehicle/viewvehicle.module';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      VehiclesRoutingModule,
      AllvehiclesModule,
      ViewvehicleModule,
  ]
})
export class VehiclesModule { }
