import {AlertModule} from '../../../_directives/alert/alert.module';
import {AllvehiclesComponent} from './allvehicles.component';
import { DataTablesModule } from 'angular-datatables';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
      AllvehiclesComponent
  ],
  imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule,
      AlertModule,
      DataTablesModule
  ]
})
export class AllvehiclesModule { }
