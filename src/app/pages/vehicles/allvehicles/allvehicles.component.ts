import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';
import {Vehicle} from '../../../_models/vehicle';
import {VehicleService} from '../../../_services/vehicle.service';

@Component({
  templateUrl: './allvehicles.component.html',
  styleUrls: ['./allvehicles.component.css']
})
export class AllvehiclesComponent implements OnInit {
  private vehicles: Vehicle[];
  private dtOptions: DataTables.Settings = {};

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  constructor(
      private router: Router,
      private vehicleService: VehicleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

      this.populate();
  }

  populate() {
      this.vehicleService.getAll().forEach((vehicles) => {
          this.vehicles = vehicles;
          this.dtTrigger.next();
      }).catch((error) => {
          this.alertService.error(error.error);
      });
  }
}
