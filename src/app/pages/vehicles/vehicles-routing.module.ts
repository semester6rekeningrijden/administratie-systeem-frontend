import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AllvehiclesComponent} from './allvehicles/allvehicles.component';
import {ViewvehicleComponent} from './viewvehicle/viewvehicle.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', component: AllvehiclesComponent, pathMatch: 'full' },
                    { path: ':license', component: ViewvehicleComponent, pathMatch: 'full' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class VehiclesRoutingModule {}
