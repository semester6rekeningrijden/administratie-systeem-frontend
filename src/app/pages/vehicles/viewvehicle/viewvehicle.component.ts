import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';
import {Vehicle} from '../../../_models/vehicle';
import {VehicleService} from '../../../_services/vehicle.service';
import {Role} from '../../../_models/role';
import {Journey} from '../../../_models/journey';

@Component({
  templateUrl: './viewvehicle.component.html',
  styleUrls: ['./viewvehicle.component.css']
})
export class ViewvehicleComponent implements OnInit {
  private vehicle: Vehicle;
  private dtOptions: DataTables.Settings = {};

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  private licensePlate: string;
    showDataModal: boolean = false;
    selectedToShowData: Journey;

  constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private vehicleService: VehicleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.activatedRoute.paramMap.forEach((params) => {
          this.licensePlate = params.get("license");
      }).catch((error) => {
          this.alertService.error(error.error);
          this.router.navigate(["/vehicles"]);
      });

      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

      this.populate();
  }

  populate() {
      this.vehicleService.getByLicensePlate(this.licensePlate).forEach((vehicle) => {
          this.vehicle = vehicle;
          this.dtTrigger.next();
      }).catch((error) => {
          this.alertService.error(error.error);
      });
  }

    setShowDataModal(journey: Journey){
        this.selectedToShowData = journey;
        this.showDataModal = true;
    }
}
