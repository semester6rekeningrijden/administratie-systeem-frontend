import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';

@Component({
  selector: 'app-allusers',
  templateUrl: './allroles.component.html',
  styleUrls: ['./allroles.component.css']
})
export class AllrolesComponent implements OnInit {
  private roles: Role[];
  private dtOptions: DataTables.Settings = {};
  showDeleteModal: boolean = false;
  selectedToDelete: Role;

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  constructor(
      private router: Router,
      private roleService: RoleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

    this.roleService.getAll().forEach((roles) => {
      this.roles = roles;
      this.dtTrigger.next();
    }).catch((error) => {
       this.alertService.error(error.error);
    });
  }

    setDeleteModal(role: Role){
        this.selectedToDelete = role;
        this.showDeleteModal = true;
    }

    deleteRole(role: Role) {
      this.roleService.delete(role).forEach((roles) => {
          this.alertService.success("Role " + role.name + "has been deleted", true);
          this.router.navigate(["/roles"]);
      }).catch((error) => {
          this.alertService.error(error.error.text, true);
          this.router.navigate(["/roles"]);
      })
    }
}
