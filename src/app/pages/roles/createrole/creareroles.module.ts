import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {AlertModule} from '../../../_directives/alert/alert.module';
import {CreaterolesComponent} from './createroles.component';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
  declarations: [
      CreaterolesComponent
  ],
  imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule,
      AlertModule
  ]
})
export class CrearerolesModule { }
