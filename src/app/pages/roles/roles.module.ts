import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RolesRoutingModule} from './roles-routing.module';
import {AllrolesModule} from './allroles/allroles.module';
import {UpdaterolesModule} from './updaterole/updateroles.module';
import {CrearerolesModule} from './createrole/creareroles.module';
import {AttachpermissionsModule} from './attachpermissions/attachpermissions.module';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      RolesRoutingModule,
      AllrolesModule,
      UpdaterolesModule,
      CrearerolesModule,
      AttachpermissionsModule,
  ]
})
export class RolesModule { }
