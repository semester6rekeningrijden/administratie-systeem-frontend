import { Component, OnInit } from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../_models';
import {Observable} from 'rxjs/internal/Observable';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';

@Component({
  selector: 'app-updateusers',
  templateUrl: './updateroles.component.html',
  styleUrls: ['./updateroles.component.css']
})
export class UpdaterolesComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    private roleId: number;
    private role: Observable<Role>;

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
      private roleService: RoleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.forEach((params) => {
      this.roleId = parseInt(params.get("id"));
    }).catch((error) => {
      this.alertService.error(error.error);
      this.router.navigate(["/roles"]);
    });

    this.role = this.roleService.getById(this.roleId);

      this.registerForm = this.formBuilder.group({
          id: ['', Validators.required],
          name: ['', Validators.required],
      });

      this.role.forEach((role) => {
          this.registerForm = this.formBuilder.group({
              id: [role.id, Validators.required],
              name: [role.name, Validators.required],
          });
      }).catch((error) => {
          this.alertService.error(error.error);
          this.router.navigate(["/roles"]);
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        if(this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.roleService.update(this.registerForm.value).forEach((data) => {
            this.alertService.success('Role Updated', true);
            this.router.navigate(['/roles']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

}
