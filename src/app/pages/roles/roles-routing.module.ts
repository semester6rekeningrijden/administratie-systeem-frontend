import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AllrolesComponent} from './allroles/allroles.component';
import {UpdaterolesComponent} from './updaterole/updateroles.component';
import {CreaterolesComponent} from './createrole/createroles.component';
import {AttachpermissionsComponent} from './attachpermissions/attachpermissions.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', component: AllrolesComponent, pathMatch: 'full' },
                    { path: 'update/:id', component: UpdaterolesComponent, pathMatch: 'full' },
                    { path: 'create', component: CreaterolesComponent, pathMatch: 'full' },
                    { path: ':id/permissions', component: AttachpermissionsComponent, pathMatch: 'full' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class RolesRoutingModule {}
