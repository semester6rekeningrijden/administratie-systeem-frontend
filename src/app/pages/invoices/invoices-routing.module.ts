import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AllinvoicesComponent} from './allinvoices/allinvoices.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', component: AllinvoicesComponent, pathMatch: 'full' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class InvoicesRoutingModule {}
