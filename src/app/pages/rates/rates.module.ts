import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RatesRoutingModule} from './rates-routing.module';
import {AllratesModule} from './allrates/allrates.module';
import {CreateratesModule} from './createrates/createrates.module';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      RatesRoutingModule,
      AllratesModule,
      CreateratesModule,
  ]
})
export class RatesModule { }
