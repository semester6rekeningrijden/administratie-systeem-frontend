import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';
import {Rate} from '../../../_models/rate';
import {RateService} from '../../../_services/rate.service';

@Component({
  templateUrl: './allrates.component.html',
  styleUrls: ['./allrates.component.css']
})
export class AllratesComponent implements OnInit {
  private rates: Rate[];
  private dtOptions: DataTables.Settings = {};
  showDeleteModal: boolean = false;
  selectedToDelete: Rate;

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  constructor(
      private router: Router,
      private rateService: RateService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

      this.populate();
  }

  populate() {
      this.rateService.getAll().forEach((rates) => {
          this.rates = rates;
          this.dtTrigger.next();
      }).catch((error) => {
          this.alertService.error(error.error);
      });
  }

    setDeleteModal(rate: Rate){
        this.selectedToDelete = rate;
        this.showDeleteModal = true;
    }

    deleteRate(rate: Rate) {
      this.rateService.delete(rate).forEach((rates) => {
          this.alertService.success("Rate " + rate.id + "has been deleted", true);
          this.populate();
      }).catch((error) => {
          this.alertService.error(error.error.text, true);
          this.router.navigate(["/rates"]);
      })
    }
}
