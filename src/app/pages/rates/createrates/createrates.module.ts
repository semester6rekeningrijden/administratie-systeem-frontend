import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {AlertModule} from '../../../_directives/alert/alert.module';
import {CreateratesComponent} from './createrates.component';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
  declarations: [
      CreateratesComponent
  ],
  imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule,
      AlertModule,
      DlDateTimeDateModule,
      DlDateTimePickerModule,
      NgSelectModule
  ]
})
export class CreateratesModule { }
