import { Component, OnInit } from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../_models';
import {Observable} from 'rxjs/internal/Observable';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';
import {RateService} from '../../../_services/rate.service';
import {RoadType} from '../../../_models/roadType';
import {VehicleType} from '../../../_models/vehicleType';
import {RecurringType} from '../../../_models/recurringType';
@Component({
  templateUrl: './createrates.component.html',
  styleUrls: ['./createrates.component.css']
})
export class CreateratesComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    vehicleType: String[];
    roadType: String[];
    recurring: String[];

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
      private rateService: RateService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          price: ['', Validators.required],
          startDate: ['', Validators.required],
          endDate: ['', Validators.required],
          vehicleType: [''],
          roadType: [''],
          recurring: ['']
      });

      this.vehicleType = Object.keys(VehicleType).filter(
          (type) => isNaN(<any>type) && type !== 'values'
      );

      this.roadType = Object.keys(RoadType).filter(
          (type) => isNaN(<any>type) && type !== 'values'
      );

      this.recurring = Object.keys(RecurringType).filter(
          (type) => isNaN(<any>type) && type !== 'values'
      );
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        if(this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.rateService.create(this.registerForm.value).forEach((data) => {
            this.alertService.success('Rate Created', true);
            this.router.navigate(['/rates']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }
}
