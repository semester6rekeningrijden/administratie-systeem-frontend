import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AllratesComponent} from './allrates/allrates.component';
import {CreateratesComponent} from './createrates/createrates.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', component: AllratesComponent, pathMatch: 'full' },
                    { path: 'create', component: CreateratesComponent, pathMatch: 'full' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class RatesRoutingModule {}
