import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllusersComponent } from './allusers/allusers.component';
import {UsersRoutingModule} from './users-routing.module';
import {AllUsersModule} from './allusers/allusers.module';
import {UpdateusersModule} from './updateuser/updateusers.module';
import {AssignroleModule} from './assignrole/assignrole.module';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      UsersRoutingModule,
      AllUsersModule,
      UpdateusersModule,
      AssignroleModule,
  ]
})
export class UsersModule { }
