import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AllusersComponent} from './allusers/allusers.component';
import {UpdateusersComponent} from './updateuser/updateusers.component';
import {AssignroleComponent} from './assignrole/assignrole.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', component: AllusersComponent, pathMatch: 'full' },
                    { path: 'update/:id', component: UpdateusersComponent, pathMatch: 'full' },
                    { path: ':id/role', component: AssignroleComponent, pathMatch: 'full' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class UsersRoutingModule {}
