import { Component, OnInit } from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../_models';
import {Observable} from 'rxjs/internal/Observable';

@Component({
  selector: 'app-updateusers',
  templateUrl: './updateusers.component.html',
  styleUrls: ['./updateusers.component.css']
})
export class UpdateusersComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    private countries: any;
    private userId: number;
    private user: Observable<User>;

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
    private userService: UserService,
      private countryService: CountryService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.forEach((params) => {
      this.userId = parseInt(params.get("id"));
    }).catch((error) => {
      this.alertService.error(error.error);
      this.router.navigate(["/users"]);
    });

    this.user = this.userService.getById(this.userId);
    this.countries = this.countryService.getAll();

      this.registerForm = this.formBuilder.group({
          id: ['', Validators.required],
          email: ['', Validators.required],
          password: ['', []],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          street: ['', Validators.required],
          city: ['', Validators.required],
          zipcode: ['', Validators.required],
          country: ['Netherlands', Validators.required],
      });

      this.user.forEach((user) => {
          this.registerForm = this.formBuilder.group({
              id: [user.id, Validators.required],
              email: [user.email, Validators.required],
              password: ['', []],
              firstName: [user.firstName, Validators.required],
              lastName: [user.lastName, Validators.required],
              street: [user.street, Validators.required],
              city: [user.city, Validators.required],
              zipcode: [user.zipcode, Validators.required],
              country: [user.country, Validators.required],
          });
      }).catch((error) => {
          this.alertService.error(error.error);
          this.router.navigate(["/users"]);
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        if(this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.update(this.registerForm.value).forEach((data) => {
            this.alertService.success('User Updated', true);
            this.router.navigate(['/users']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

}
