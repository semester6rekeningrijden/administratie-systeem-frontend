import {AlertModule} from '../../../_directives/alert/alert.module';
import {AllusersComponent} from './allusers.component';
import { DataTablesModule } from 'angular-datatables';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
      AllusersComponent
  ],
  imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule,
      AlertModule,
      DataTablesModule
  ]
})
export class AllUsersModule { }
