import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {AuthGuard} from '../../_guards';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: '', redirectTo: 'login', pathMatch: 'full' },
                    { path: 'login', component: LoginComponent, pathMatch: 'full' },
                    { path: 'register', component: RegisterComponent, pathMatch: 'full', canActivate: [AuthGuard] },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
