import {Component, OnInit} from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService} from '../../../_services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  private countries: any;

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private userService: UserService,
      private countryService: CountryService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.countries = this.countryService.getAll();

      this.registerForm = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          street: ['', Validators.required],
          city: ['', Validators.required],
          zipcode: ['', Validators.required],
          country: ['Netherlands', Validators.required],
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
      this.submitted = true;

      if(this.registerForm.invalid) {
          return;
      }

        this.loading = true;
        this.userService.register(this.registerForm.value).forEach((data) => {
            this.alertService.success('Registration successful', true);
            this.router.navigate(['/users']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }
}
