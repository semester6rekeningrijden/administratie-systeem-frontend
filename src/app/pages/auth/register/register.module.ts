import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegisterComponent} from './register.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertModule} from '../../../_directives/alert/alert.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
      RegisterComponent
  ],
  imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule,
      AlertModule,
      NgSelectModule
  ]
})
export class RegisterModule { }
