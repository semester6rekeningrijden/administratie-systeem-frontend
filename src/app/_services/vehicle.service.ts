import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import {REGISTRATION_V1_API} from '../_helpers/api-constants';
import {Vehicle} from '../_models/vehicle';

@Injectable({ providedIn: 'root' })
export class VehicleService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Vehicle[]>(REGISTRATION_V1_API("vehicles"));
    }

    getById(id: number) {
        return this.http.get<Vehicle>(REGISTRATION_V1_API("vehicles/" + id));
    }

    getByLicensePlate(license: string) {
        return this.http.get<Vehicle>(REGISTRATION_V1_API("vehicles/license/" + license));
    }
}