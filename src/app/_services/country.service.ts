import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient, HttpHeaders} from '@angular/common/http';


import { User } from '../_models';
import {COUNTRY_API, ADMINISTRATION_V1_API} from '../_helpers/api-constants';

@Injectable({ providedIn: 'root' })
export class CountryService {
    private http: HttpClient;

    constructor(
        private handler: HttpBackend
    ) {
        this.http = new HttpClient(handler);
    }

    getAll() {
        return this.http.get<any>(COUNTRY_API("all"));
    }
}