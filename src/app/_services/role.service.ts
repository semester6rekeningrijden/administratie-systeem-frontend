import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import {ADMINISTRATION_V1_API} from '../_helpers/api-constants';
import {Role} from '../_models/role';

@Injectable({ providedIn: 'root' })
export class RoleService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Role[]>(ADMINISTRATION_V1_API("roles"));
    }

    getById(id: number) {
        return this.http.get<Role>(ADMINISTRATION_V1_API("roles/" + id));
    }

    getByName(name: number) {
        return this.http.get<Role>(ADMINISTRATION_V1_API("roles/name/" + name));
    }

    create(role: Role) {
        return this.http.post<Role>(ADMINISTRATION_V1_API("roles"), role);
    }

    update(role: Role) {
        return this.http.patch<Role>(ADMINISTRATION_V1_API("roles/" + role.id), role);
    }

    delete(role: Role) {
        return this.http.delete(ADMINISTRATION_V1_API("roles/" + role.id));
    }

    attach(roleId: number, permissionId: number) {
        return this.http.patch<Role>(ADMINISTRATION_V1_API("roles/" + roleId + "/attach"), {
            roleId: roleId,
            permissionId: permissionId,
        });
    }

    detach(roleId: number, permissionId: number) {
        return this.http.patch<Role>(ADMINISTRATION_V1_API("roles/" + roleId + "/detach"), {
            roleId: roleId,
            permissionId: permissionId,
        });
    }
}