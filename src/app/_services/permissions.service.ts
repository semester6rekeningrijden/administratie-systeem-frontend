import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {ADMINISTRATION_V1_API} from '../_helpers/api-constants';
import {Permission} from '../_models/permission';

@Injectable({ providedIn: 'root' })
export class PermissionsService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Permission[]>(ADMINISTRATION_V1_API("permissions"));
    }
}