import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {ADMINISTRATION_V1_API} from '../_helpers/api-constants';
import {UserService} from './user.service';
import {User} from '../_models';
import {Observable} from 'rxjs/internal/Observable';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(
        private http: HttpClient,
        private userService: UserService,
    ) { }

    login(email: string, password: string) {
        return this.http.post<any>(ADMINISTRATION_V1_API("auth/login"), {
            email: email,
            password: password
        }).pipe(map(user => {
            if(user && user.token && user.userId) {
                localStorage.setItem('currentUser', JSON.stringify(user));
            }

            return user;
        }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    getLoggedInUser() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if(currentUser && currentUser.token && currentUser.userId) {
            this.userService.getById(currentUser.userId).pipe(map(u => {
                if(u instanceof User) {
                    if(u.id == currentUser.userId) {
                        return u;
                    } else {
                        this.logout();
                    }
                } else {
                    this.logout();
                }
            }));
        }
    }
}