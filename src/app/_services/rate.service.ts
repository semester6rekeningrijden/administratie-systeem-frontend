import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import {ADMINISTRATION_V1_API} from '../_helpers/api-constants';
import {Role} from '../_models/role';
import {Rate} from '../_models/rate';
import {RoadType} from '../_models/roadType';
import {VehicleType} from '../_models/vehicleType';
import * as moment from 'moment';
import {RecurringType} from '../_models/recurringType';
@Injectable({ providedIn: 'root' })
export class RateService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Rate[]>(ADMINISTRATION_V1_API("rates"));
    }

    getById(id: number) {
        return this.http.get<Rate>(ADMINISTRATION_V1_API("rates/" + id));
    }

    create(rate: Rate) {
        let ldtf = "YYYY-MM-DDTHH:mm:ss";

        return this.http.post<Rate>(ADMINISTRATION_V1_API("rates"), {
            price: rate.price,
            vehicleType: rate.vehicleType != undefined && rate.vehicleType != VehicleType.ALL ? rate.vehicleType : null,
            roadType: rate.roadType != undefined && rate.roadType != RoadType.ALL ? rate.roadType : null,
            startDate: moment.utc(rate.startDate, ldtf).local().format(ldtf),
            endDate: moment.utc(rate.endDate, ldtf).local().format(ldtf),
            recurring: rate.recurring != undefined && rate.recurring != RecurringType.NONE ? rate.recurring : null
        });
    }

    update(rate: Rate) {
        return this.http.patch<Rate>(ADMINISTRATION_V1_API("rates/" + rate.id), rate);
    }

    delete(rate: Rate) {
        return this.http.delete(ADMINISTRATION_V1_API("rates/" + rate.id));
    }
}